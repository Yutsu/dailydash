# DailyDash Website
![Design homepage of the DailyDash Website](./public/assets/homepage.png)

## Description
This is a DaidyDash website project developed using react. In this project, you
can have the daily of the spaceflight, search the weather of a city, check the cryptocurrency curve, see all the covid19 information and lastly see your birthday friends.

## Technologies Used
React

Hooks

Redux

Scss

React-icons

ChartJS

i18next

Leaflet

Axios

## Installation

git clone https://gitlab.com/Yutsu/dailydash

npm install

npm start