import React, { Suspense } from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import App from './App';
import store from './redux/rootReducer';
import './settings.scss';
import './pages/Meteo/Translation/locales/i18n';

ReactDOM.render(
    <React.StrictMode>
        <Provider store={store}>
            <BrowserRouter>
                <Suspense fallback={(<div>Loading</div>)}>
                    <App />
                </Suspense>
            </BrowserRouter>
        </Provider>
    </React.StrictMode>,
    document.getElementById('root')
);