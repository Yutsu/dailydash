import { TextField } from '@material-ui/core';
import React from 'react';
import './styles.scss';

const FormSelect = ({ options, onChange, label, value, ...props}) => {
    if (!Array.isArray(options) || options.length < 1) return null;

    return (
        <div className="formRow_select">
                <TextField
                    id="outlined-select-currency-native"
                    select
                    label={label}
                    options={options}
                    SelectProps={{native: true}}
                    variant="outlined"
                    onChange={onChange}
                    className="select"
                    value={value}
                    { ...props}
                >
                    {options.map((option) => (
                        <option key={option.value} value={option.value}>
                            {option.label}
                        </option>
                    ))}
                </TextField>
        </div>
    );
}

export default FormSelect;