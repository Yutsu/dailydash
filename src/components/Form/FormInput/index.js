import React from 'react';
import './styles.scss';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
    root: {
        width: '100%',
    }
});

const FormInput = ({onChange, label, ...props}) => {
    const classes = useStyles();
    return (
        <div className="formRow_input">
            <TextField
                id="outlined-basic"
                className={classes.root}
                label={label}
                color="primary"
                variant="outlined"
                onChange={onChange}
                { ...props}
            />
        </div>
    );
};

export default FormInput;