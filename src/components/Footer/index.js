import React from 'react';
import {useSelector } from 'react-redux';

import './styles.scss';

const mapState = ({navClick}) => ({
    bigNav: navClick.big,
});

const Footer = () => {
    const { bigNav } = useSelector(mapState);

    return (
        <footer className={bigNav ? "" : "toggleBig"}>
            <h2>Footer</h2>
        </footer>
    );
};

export default Footer;