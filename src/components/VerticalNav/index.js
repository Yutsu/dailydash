import React from 'react';
import { TiWeatherCloudy } from "react-icons/ti";
import { GiSyringe } from "react-icons/gi";
import { BiRocket } from "react-icons/bi";
import { MdCake } from "react-icons/md";
import { BsFillPersonFill } from "react-icons/bs";
import { FaMoneyBillWave } from "react-icons/fa";
import './styles.scss';
import { NavLink } from 'react-router-dom';
import { useSelector } from 'react-redux';

const mapState = ({navClick}) => ({
    bigNav: navClick.big,
    smallNav: navClick.small
});

const VerticalNav = () => {
    const { bigNav, smallNav } = useSelector(mapState);

    return (
        <div className={smallNav ? "go_right" : "go_left"}>
            <aside className={bigNav ? "vertical-nav" : "vertical-nav only-icon"}>
                <h2 className={bigNav ? "title" : "hide"}>DailyDay</h2>
                <h2 className={bigNav ? "hide" : "title"}>DD</h2>
                <div className="nav-menu">
                    <div className="profile">
                        <BsFillPersonFill />
                        <p className={bigNav ? "block" : "hide"}>Someone</p>
                    </div>
                    <div className="menu">
                        <p className={bigNav ? "separator" : "hide"}>Menu</p>
                        <ul>
                            <li>
                                <NavLink exact={true} to="/" activeClassName='active'>
                                    <BiRocket/>
                                    <span className={bigNav ? "block" : "hide"}>News</span>
                                </NavLink>
                            </li>
                            <li>
                                <NavLink to="/meteo" activeClassName='active'>
                                    <TiWeatherCloudy/>
                                    <span className={bigNav ? "block" : "hide"}>Météo</span>
                                </NavLink>
                            </li>
                            <li>
                                <NavLink to="/crypto" activeClassName='active'>
                                    <FaMoneyBillWave />
                                    <span className={bigNav ? "block" : "hide"}>Crypto</span>
                                </NavLink>
                            </li>
                            <li>
                                <NavLink to="/covid" activeClassName='active'>
                                    <GiSyringe />
                                    <span className={bigNav ? "block" : "hide"}>Covid19</span>
                                </NavLink>
                            </li>
                            <li>
                                <NavLink to="/bday" activeClassName='active'>
                                    <MdCake />
                                    <span className={bigNav ? "block" : "hide"}>B-day</span>
                                </NavLink>
                            </li>
                        </ul>
                    </div>
                </div>
            </aside>
        </div>
    );
};

export default VerticalNav;