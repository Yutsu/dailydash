import React, { useState } from 'react';
import { BsFillPersonFill } from "react-icons/bs";
import { useDispatch, useSelector } from 'react-redux';
import { toggleNavBigAction, toggleNavSmallAction } from '../../redux/Nav/actions/nav';
import './styles.scss';

const mapState = ({navClick}) => ({
    bigNav: navClick.big,
    smallNav: navClick.small
});

const Header = () => {
    const { bigNav, smallNav } = useSelector(mapState);
    const [width, setWidth] = useState(window.innerWidth);
    const [toggleSmallScreen, setToggleSmallScreen] = useState(smallNav);
    const [toggleBigScreen, setToggleBigScreen] = useState(bigNav);
    const dispatch = useDispatch();


    const toggleNav = () => {
        if(width <= 890){
            // console.log("Menu gauche à droite " + width);
            setToggleSmallScreen(!toggleSmallScreen);
            dispatch(toggleNavSmallAction(toggleSmallScreen));
            setWidth(window.innerWidth);
        } else {
            // console.log("Menu droite à gauche en petit " + width);
            setToggleBigScreen(!toggleBigScreen)
            dispatch(toggleNavBigAction(toggleBigScreen));
            setWidth(window.innerWidth);
        }
    }

    return <header>
        <div className={smallNav ? 'block' : "hide"}>
            <div className="logo-website">
                DailyDash
            </div>
        </div>
        <div className="menu">
            <div className="hamburger" onClick={toggleNav}>
                <div className="line"></div>
                <div className="line"></div>
                <div className="line"></div>
            </div>
                <div className="profile">
                    <BsFillPersonFill />
                    <p>Someone</p>
                </div>
        </div>
    </header>
};

export default Header;