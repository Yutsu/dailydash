import axios from 'axios';

export const coinGecko = axios.create({
    baseURL: "https://api.coingecko.com/api/v3"
});

export const covidApi = axios.create({
    baseURL: "https://disease.sh/v3/covid-19"
});

export const spaceApi = axios.create({
    baseURL: "https://spaceflightnewsapi.net/api/v2"
});

export const birthdayJsonApi = axios.create({
    baseURL: "https://jsonplaceholder.typicode.com"
})
