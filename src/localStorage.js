import { mapBirthday } from './util';

const checkBirthday = () => {
    let birthdays;
    let storage = localStorage.getItem('birthdayInfo');
    storage === null ? birthdays = [] : birthdays = JSON.parse(storage);
    return birthdays;
}

export const saveLocalBirthday = (birthdayInfo) => {
    let birthdays = checkBirthday();
    birthdays.push(birthdayInfo);
    localStorage.setItem('birthdayInfo', JSON.stringify(birthdays));
}

export const deleteLocalBirthday = (idBirthday) => {
    let birthday = checkBirthday();
    let index = birthday.map(i => i.id).indexOf(idBirthday);
    birthday.splice(index, 1);
    localStorage.setItem('birthdayInfo', JSON.stringify(birthday));
}

export const editLocalBirthday = (idBirthday, result) => {
    let birthday = checkBirthday();
    let test = mapBirthday(birthday, idBirthday, result);
    localStorage.setItem('birthdayInfo', JSON.stringify(test));
}