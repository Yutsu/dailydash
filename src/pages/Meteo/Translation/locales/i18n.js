
import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

import translationFR from './fr/translationFR.json';
import translationEN from './en/translationEN.json';

const resources = {
    en: { translation: translationEN },
    fr: { translation: translationFR }
};

i18n
    .use(initReactI18next)
    .init({
        resources,
        lng: 'fr',
        keySeparator: false,
        interpolation: {
            escapeValue: false
        }
    })
export default i18n;