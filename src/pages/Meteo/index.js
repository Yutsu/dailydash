import axios from 'axios';
import moment from 'moment';
import React, { useState } from 'react';
import LineChart from './Components/LineChart';
import FormInput from '../../components/Form/FormInput';
import FormSelect from '../../components/Form/FormSelect';
import NextDays from './Options/NextDays';
import Today from './Options/Today';
import 'moment/locale/fr';
import './styles.scss';
import Favoris from './Options/Favoris';
import { useTranslation } from 'react-i18next';
import i18n from './Translation/locales/i18n';
import { Button } from '@material-ui/core';

const daysFr = [
    { value: 'aujourdhui', label: "Aujourd'hui" },
    { value: "prochain_jours", label: "Prochains jours" },
    { value: "favoris", label: "Ville favoris" }
];

const daysEn = [
    { value: 'aujourdhui', label: "Today" },
    { value: "prochain_jours", label: "Next days" },
    { value: "favoris", label: "Favorite city" }
];

const onlyPair = (tab) => {
    let pair = [];
    for(let i = 0; i < tab.length; i++){
        if(i % 2 === 0){
            pair.push(tab[i]);
        }
    }
    return pair;
}

const Meteo = () => {
    const PATH_CITY = "https://api.openweathermap.org/data/2.5/weather?q=";
    const PATH_CITY_MORE = "http://api.openweathermap.org/data/2.5/forecast?q=";

    const [lang, setLang] = useState("fr");
    const API = `&appid=${process.env.REACT_APP_API_KEY_WEATHER}&units=metric&lang=`;

    const [city, setCity] = useState("");
    const [cityDisplay, setCityDisplay] = useState("");
    const [cityThree, setCityThree] = useState([]);
    const [cityHours, setCityHours] = useState([]);
    const [cityTemps, setCityTemps] = useState([]);
    const [fiveDailyData, setFiveDailyData] = useState([]);
    const [fiveDays, setFiveDays] = useState([]);
    const [titleDateFive, setTitleDateFive] = useState([]);
    const [selectOptions, setSelectOptions] = useState("aujourdhui");
    const [catchError, setCatchError] = useState(false);

    let tabCityThree = [], tabCityHours = [];
    let tabCityTemps = [], tabFiveDays = [];
    let tabDateFiveTitle = [];
    let dailyData = {};

    const getWeather = async(lang) => {
        await axios
            .get(`${PATH_CITY}${city}${API}${lang}`)
            .then(response => {
                setCityDisplay(response.data);
                setCatchError(false);
            })
            .catch(error => {
                if(error.response && error.response.status === 404) {
                    console.clear();
                    setCatchError(true);
                }
            })
    }

    const getWeatherMore = async(lang) => {
        await axios
            .get(`${PATH_CITY_MORE}${city}${API}${lang}`)
            .then(response => {
                for(let i = 0; i < 3; i++){
                    tabCityThree.push(response.data.list[i]);
                }
                setCityThree(tabCityThree);

                for(const weather of response.data.list){
                    tabCityHours.push(weather.dt_txt.substring(11, 16));
                    tabCityTemps.push(weather.main.temp);
                }
                const pairCityHours = onlyPair(tabCityHours);
                const pairCityTemps = onlyPair(tabCityTemps);
                setCityHours(pairCityHours);
                setCityTemps(pairCityTemps);

                moment.locale('fr');
                response.data.list.map(item => {
                    const dateTime = new Date(item.dt * 1000);
                    const day = dateTime.getDate();
                    const time = dateTime.getHours();
                    const timeMoment = moment(dateTime).format('L');

                    tabDateFiveTitle.push(timeMoment);
                    if(!dailyData[day])
                    dailyData[day]=[];
                    dailyData[day].push({...item,day,time});
                    tabFiveDays.push(day);
                    return dailyData
                });
                const titleDate = [...new Set(tabDateFiveTitle)];
                const days = [...new Set(tabFiveDays)];
                setTitleDateFive(titleDate);
                setFiveDays(days);
                setFiveDailyData(dailyData)
            })
            .catch(error => {
                if(error.response && error.response.status === 404) {
                    console.clear();
                }
            })
    }

    const handleFormSubmit = (e) => {
        e.preventDefault();
        if(!city) return null;
        getWeather(lang);
        getWeatherMore(lang);
        setSelectOptions('aujourdhui')
    }

    const switchDisplay = (options) => {
        switch(options) {
            case 'aujourdhui':
                return <>
                    <Today
                        dataCity={cityDisplay}
                        dataCityMore={cityThree}
                        setSelectOptions={setSelectOptions}
                    />
                    <LineChart
                        dataCityHours={cityHours}
                        dataCityTemp={cityTemps}
                    />
                </>
            case 'prochain_jours':
                return <NextDays
                    dataFiveDailyData={fiveDailyData}
                    dataFiveDays={fiveDays}
                    dataTitleDateFive={titleDateFive}
                    dataCity={cityDisplay}
                />
            case 'favoris':
                return <Favoris />
            default:
                return null;
        }
    }

    const traduction = (ln) => {
        i18n.changeLanguage(ln);
        setLang(ln);
        getWeather(ln);
        getWeatherMore(ln)
    }

    const { t } = useTranslation();

    return (
        <div className="weather">
            <form onSubmit={handleFormSubmit} className="weather_form">
                <div className="form_grid-left">
                    <FormInput
                        label={lang === "fr" ? "Ville" : "City"}
                        type="text"
                        name="city"
                        value={city}
                        placeholder={lang === "fr" ? "Recherche ta ville" : "Find your city"}
                        onChange={e => setCity(e.target.value)}
                    />
                    {cityDisplay &&
                        <FormSelect
                            options={lang === "fr" ? daysFr : daysEn}
                            onChange={e => setSelectOptions(e.target.value)}
                            value={selectOptions}
                            type="reset"
                            label="Options"
                        />
                    }
                </div>
                <div className="form_grid-right">
                    <div className="button-langage">
                        <Button className="button-click" id='target1' variant="outlined" color="primary" onClick={() => traduction('fr')}>Fr</Button>
                        <Button className="button-click" id='target2' variant="outlined" color="primary" onClick={() => traduction('en')}>En</Button>
                    </div>
                </div>
            </form>
            {!cityDisplay && !catchError &&
                <div className="weather_intro">
                    <p>{t('introduction')}</p>
                    <img src="/assets/weather.svg" className="weather_img" alt="weather"/>
                </div>
            }
            {cityDisplay && !catchError && switchDisplay(selectOptions)}
            {catchError && <div className="img_not_found">
                <img src="/assets/not-found.png" alt="error not found"/>
            </div>}
        </div>
    );
};

export default Meteo;