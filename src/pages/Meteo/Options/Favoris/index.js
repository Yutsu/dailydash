import { Paper } from '@material-ui/core';
import React, { useState } from 'react';
import { FiStar } from 'react-icons/fi';
import { useSelector, useDispatch } from 'react-redux';
import { removeCityFavAction } from '../../../../redux/Meteo/actions/cityFav';
import { useTranslation } from 'react-i18next';
import axios from 'axios';
import './styles.scss';
import BarChartFav from '../../Components/BarChart';

const mapState = ({cityFav}) => ({
    city: cityFav.cityDisplay,
});

const Favoris = () => {
    const { city } = useSelector(mapState);
    const dispatch = useDispatch();
    const { t } = useTranslation();
    const [clickCity, setClickCity] = useState("");
    const [cityHours, setCityHours] = useState([]);
    const [cityTemps, setCityTemps] = useState([]);
    let tabCityHours = [], tabCityTemps =[]

    const removeCityFav = (cityClick) => {
        dispatch(removeCityFavAction({
            cityClick,
        }));
    }

    const clickCityFav =  async(cityName) => {
        await axios
            .get(`http://api.openweathermap.org/data/2.5/forecast?q=${cityName}&appid=${process.env.REACT_APP_API_KEY_WEATHER}&units=metric`)
            .then(response => {
                console.log(response.data)
                for(let i = 0; i < 15; i++){
                    tabCityHours.push(response.data.list[i].dt_txt.substring(11, 16))
                    tabCityTemps.push(response.data.list[i].main.temp)
                }
                setCityHours(tabCityHours);
                setCityTemps(tabCityTemps);
            })
            .catch(error => {
                if(error.response && error.response.status === 404) {
                    console.clear();
                }
            })
            setClickCity(cityName)
    }

    return (
        <div className="container column">
            {city.length === 0 && <div className="fav_no-city">
                <h2>{t('fav')}</h2>
                <img className="fav_no-city_img"
                    src="/assets/like_dislike.svg"
                    alt="no like city yet by the user"
                />
            </div>}

            <div className="fav-grid">
                {city.map((cityDisplay, index) => ( cityDisplay.isFav !== false &&
                    <Paper elevation={3} className="card" key={index}>
                        <div className="card">
                            <img
                                className="fav-flag"
                                src={`https://lipis.github.io/flag-icon-css/flags/4x3/${cityDisplay.country.toLowerCase()}.svg`}
                                alt="weather logo display"
                                onClick={() => clickCityFav(cityDisplay.city)}
                            />
                            <h3 className="fav-city" onClick={() => clickCityFav(cityDisplay.city)}>{cityDisplay.city}, {cityDisplay.country}</h3>
                            <FiStar onClick={()=>removeCityFav(cityDisplay.city)}/>
                        </div>
                    </Paper>
                ))}
            </div>
            {city.length !== 0 && clickCity &&
                <BarChartFav
                    dataCityHours={cityHours}
                    dataCityTemp={cityTemps}
                    dataCityName={clickCity}
                />
            }
        </div>
    );
};

export default Favoris;