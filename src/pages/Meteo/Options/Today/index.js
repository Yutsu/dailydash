import React from 'react';
import { FiSunrise, FiSunset, FiStar } from 'react-icons/fi';
import { GiWaterDrop } from 'react-icons/gi';
import { TiWeatherWindyCloudy } from 'react-icons/ti';
import { WiDaySunnyOvercast, WiNightAltCloudy } from 'react-icons/wi';
import { useSelector, useDispatch } from 'react-redux';
import { toggleCityAction } from '../../../../redux/Meteo/actions/cityFav';
import { Button, Paper } from '@material-ui/core';
import './styles.scss';
import moment from 'moment';
import { useTranslation } from 'react-i18next';
import PropTypes from 'prop-types';
import 'moment/locale/fr';

const dayNight = (icon) => {
    if(icon.includes("d")){
        return <div className="infoDayNight">
            <p>Day</p>
            <WiDaySunnyOvercast className="icon_day-night"/>
        </div>
    }
    return <div className="infoDayNight">
        <p>Night</p>
        <WiNightAltCloudy className="icon_day-night"/>
    </div>
}

const sun = (sun) => {
    let date1 = new Date(sun*1000);
    let hours1 = "0" + date1.getHours();
    let minutes1 = "0" + date1.getMinutes();
    let format = hours1.substr(-2) + ':' + minutes1.substr(-2);
    return format;
}
const sunset = (sunsetDisplay) => {return sun(sunsetDisplay)}
const sunrise = (sunriseDisplay) => {return sun(sunriseDisplay)}

const dataDisplay = (date) => {
    const dateTime = new Date(date * 1000);
    moment.locale('fr');
    const timeMoment = moment(dateTime).format('L');
    return timeMoment;
}

const mapState = ({cityFav}) => ({
    cityStore: cityFav.cityDisplay.map(test => test.city),
});

const Today = ({dataCity, dataCityMore, setSelectOptions}) => {
    const { cityStore } = useSelector(mapState);
    const { main, name, sys, weather, wind } = dataCity;
    const countryCity = sys.country;
    const dispatch = useDispatch();
    const { t } = useTranslation();

    const toggleFav = (favCity) => {
        dispatch(toggleCityAction({
            name,
            favCity,
            countryCity
        }));
    }

    const displayFavToggle = () => {
        let result;
        if(cityStore.length === 0) {
            return result = <p className="icon star">
                <FiStar onClick={() => toggleFav(false)}/>
            </p>
        }
        if(cityStore.length !== 0){
            console.log(cityStore)
            for (let i = 0; i < cityStore.length; i++) {
                if (cityStore[i] === name) {
                    result = <p className="icon star fav">
                        <FiStar onClick={() => toggleFav(true)}/>
                    </p>
                    break;
                }
                result = <p className="icon star">
                    <FiStar onClick={() => toggleFav(false)}/>
                </p>
            }
        }
        return result;
    }

    return (
        <div className="container">
            <div className="grid left">
                <Paper elevation={3} className="card">
                    <h2>{t("now")}</h2>
                    <div className="information">
                        <div className="description">
                            <div className="image">
                                {weather.map((info, index) => (
                                    <img key={index} src={`https://openweathermap.org/img/wn/${info.icon}@2x.png`} alt="weather logo display"/>
                                ))}
                            </div>
                            <div className="info">
                                <h1>{main.temp}°</h1>
                                {weather.map((info, index) => (
                                    <p key={index}>{info.description}</p>
                                ))}
                                <p>{t('temp_min')} {main.temp_min}°</p>
                                <p>{t('temp_max')} {main.temp_max}°</p>
                                <p>{t('feeling')} {main.feels_like}°</p>
                                <p>{t('atmospheric')} {main.pressure} hPa</p>
                            </div>
                        </div>
                        <div className="country">
                            {displayFavToggle()}
                            <p>{name}, {countryCity}</p>
                            <p>{moment().format('L')}</p>
                            <img
                                className="flag"
                                src={`https://lipis.github.io/flag-icon-css/flags/4x3/${countryCity.toLowerCase()}.svg`}
                                alt="weather logo display"
                            />
                            {weather.map((info, index) => (
                                <div key={index} className="icon">{dayNight(info.icon)}</div>
                            ))}
                        </div>
                    </div>
                </Paper>
                <Paper elevation={3} className="card">
                    <div className="card_next-days">
                        <h2>{t('next_hours')}</h2>
                        <Button
                            variant="contained"
                            color="primary"
                            onClick={() => setSelectOptions('prochain_jours')}
                        >
                            {t('see_more')}
                        </Button>
                    </div>
                    <div className="grid-days">
                        {dataCityMore.map((info, index )=> (
                            <div className="card" key={index}>
                                <h2>{info.main.temp}°</h2>
                                <img src={`https://openweathermap.org/img/wn/${info.weather[0].icon}@2x.png`} alt="weather logo display"/>
                                <h4>{info.dt_txt.substring(11, 16)}</h4>
                                <h4>{dataDisplay(info.dt)}</h4>
                            </div>
                        ))}
                    </div>
                </Paper>
            </div>
            <div className="grid-two right">
                <Paper elevation={3} className="card">
                    <h2>{t('wind')}</h2>
                    <div className="wind center">
                        <p>{wind.speed}m/s</p>
                        <TiWeatherWindyCloudy className="image"/>
                    </div>
                </Paper>
                <Paper elevation={3} className="card">
                    <h2>{t('humidity')}</h2>
                    <div className="wind center">
                        <p>{main.humidity}%</p>
                        <GiWaterDrop className="image"/>
                    </div>
                </Paper>
                <Paper elevation={3} className="card">
                        <h2>{t('sun')}</h2>
                    <div className="sunset-sunrise">
                        <div className="sun-icons">
                            <div className="images">
                                <FiSunrise />
                            </div>
                            <div className="text">
                                <h4>{t('sunrise')}</h4>
                                <h3>{sunrise(sys.sunrise)}</h3>
                            </div>
                        </div>
                        <div className="sun-icons">
                            <div className="images">
                                <FiSunset />
                            </div>
                            <div className="text">
                                <h4>{t('sunset')}</h4>
                                <h3>{sunset(sys.sunset)}</h3>
                            </div>
                        </div>
                    </div>
                </Paper>
            </div>
        </div>
    );
};

Today.propTypes = {
    dataCity: PropTypes.shape({
        main: PropTypes.object,
        name: PropTypes.string,
        sys: PropTypes.object,
        weather: PropTypes.array,
        wind: PropTypes.object
    }),
    dataCityMore: PropTypes.array,
    setSelectOptions: PropTypes.func
}

export default Today;