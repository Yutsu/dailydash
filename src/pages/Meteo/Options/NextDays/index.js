import { Paper } from '@material-ui/core';
import React, {useEffect} from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import './styles.scss';

const localeData = (dataFiveDays, dataFiveDailyData, dataTitleDateFive) => {
    const test = dataFiveDays.map((date, index, key) => {
        const title =
            <div key={index} className="next-day_date">
                <p>{dataTitleDateFive[index]}</p>
            </div>
        const description = dataFiveDailyData[date].map((info, index) => (
            <Paper key={index} className="next-day_paper">
                <div className="next-day_description">
                    {info.weather.map((dayWeather, index) => (
                        <span key={index}>{dayWeather.description}</span>
                    ))}
                    <h2>{info.main.temp}°</h2>
                    <img src={`https://openweathermap.org/img/wn/${info.weather[0].icon}@2x.png`} alt="weather logo display"/>
                    <p>{info.dt_txt.substring(11,16)}</p>
                </div>
            </Paper>
        ));
        const information = <div className="next-day_card" key={key}>{description}</div>
        return ([title, information]);
    })
    return test;
}

const NextDays = ({dataFiveDailyData, dataFiveDays, dataTitleDateFive, dataCity}) => {
    useEffect(() => {
        window.scrollTo(0, 0);
    }, []);

    const { t } = useTranslation();

    return (
        <div className="container">
            <div className="next-day_information">
                <h2 className="next-day_title">{t('next_days')} {dataCity.name}</h2>
                {localeData(dataFiveDays, dataFiveDailyData, dataTitleDateFive)}
            </div>
        </div>
    );
};

NextDays.propTypes = {
    dataFiveDays: PropTypes.array,
    dataFiveDailyData: PropTypes.object,
    dataTitleDateFive: PropTypes.array
}

export default NextDays;