import React from 'react';
import { Bar } from 'react-chartjs-2';
import { useTranslation } from 'react-i18next';

const BarChartFav = ({dataCityHours, dataCityTemp, dataCityName}) => {
    const { t } = useTranslation();

    const options = {
        title: {
            display: true,
            text: `${t('barChart')} ${dataCityName}`
        },
        scales: {
            yAxes: [{
                ticks: {
                    min: 0,
                    max: 30,
                    stepSize: 1
                }
            }]
        }
    }
    const data = {
        labels: dataCityHours,
        datasets: [{
            label: `${t('temperatureChart')}`,
            data: dataCityTemp,
            tension: 0.1,
            backgroundColor: 'orange',
            pointBackgroundColor: 'black',
            pointBorderColor: 'orange',
            borderWidth: 5
        }]
    };

    return (
        <div className="chartJS">
            <Bar
                options={options}
                data={data}
                height={50}
                width={100}
            />
        </div>
    );
};

export default BarChartFav;