import React from 'react';
import { useTranslation } from 'react-i18next';
import { Line } from 'react-chartjs-2';

const LineChart = ({dataCityHours, dataCityTemp}) => {
    const { t } = useTranslation();

    const options = {
        title: {
            display: true,
            text: `${t('lineChart')}`
        },
        scales: {
            yAxes: [{
                ticks: {
                    min: 0,
                    max: 25,
                    stepSize: 1
                }
            }]
        }
    }
    const data = {
        labels: dataCityHours,
        datasets: [{
            label: `${t('temperatureChart')}`,
            data: dataCityTemp,
            tension: 0.1,
            borderColor: ['rgb(75, 192, 192)'],
            backgroundColor: 'orange',
            pointBackgroundColor: 'black',
            pointBorderColor: 'orange',
            borderWidth: 5
        }]
    };

    return (
        <div className="chartJS">
            <Line
                options={options}
                data={data}
                height={50}
                width={100}
            />
        </div>
    );
};

export default LineChart;