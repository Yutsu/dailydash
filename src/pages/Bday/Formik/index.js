import { Button } from '@material-ui/core';
import { Form, Formik } from 'formik';
import React from 'react';
import { TextFieldForm } from '../TextField';
import './styles.scss';

const FormikYup = (props) => {
    return (
        <Formik
            initialValues={props.initialValues}
            validationSchema={props.validationSchema}
            onSubmit={props.onSubmit}
        >
            <Form>
                {Object.keys(props.initialValues).map((initialValues, index) => (
                    <TextFieldForm
                        key={index}
                        label={initialValues}
                        name={initialValues}
                        type="text"
                    />
                ))}
                <div className="button_list">
                    <Button variant="outlined" color="primary" className="form_button" onClick={() => props.setVisible(false)} type="submit">Register</Button>
                    <Button variant="outlined" color="primary" className="form_button" type="reset">Reset</Button>
                </div>
            </Form>
        </Formik>
    );
};

export default FormikYup;