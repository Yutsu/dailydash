import Modal from 'react-awesome-modal';
import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { birthdayJsonApi } from './../../../components/Api';
import { mapBirthday, validationSchema } from './../../../util';
import { editLocalBirthday, deleteLocalBirthday } from './../../../localStorage';
import FormikYup from '../Formik';

const useStyles = makeStyles({
    root: {
        minWidth: 275,
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
    name: {
        fontSize: 19,
    },
    action: {
        display: "flex",
        justifyContent: "right",
    }
});

const CardsBirthday = ({data, dataAll, setInfoBirthday, setInfoJson, infoJson}) => {
    const classes = useStyles();
    const bull = <span className={classes.bullet}>•</span>;
    const { id, name, username, address } = data;
    const { street, suite, city } = address;
    const [visible, setVisible] = useState(false);

    const initialValues = {
        name: name,
        date: username,
        age: street,
        gift: suite,
        price: city
    }

    const onSubmit = async(values) => {
        const dataValue = {
            name: values.name,
            username: values.date,
            address: {
                street: values.age,
                suite: values.gift,
                city: values.price
            }
        }

        await birthdayJsonApi
            .put(`/users/${id}`, dataValue)
            .then(response => {
                const result = response.data;
                const update1 = mapBirthday(infoJson, id, result);
                const update2 = mapBirthday(dataAll, id, result);
                editLocalBirthday(id, result);
                setInfoJson(update1);
                setInfoBirthday(update2);
                console.log(infoJson.map(infoJson => infoJson))
                console.log(dataAll.map(infoJson => infoJson))
            })
            .catch(error=>console.log(error));
    }

    const removeBirthday = async(idNumber) => {
        await birthdayJsonApi
            .delete(`/users/${idNumber}`)
            .then(response => {
                if (response.status !== 200) { return; }
                const filterRemove = dataAll.filter((birthday) => { return birthday.id !== idNumber; });
                setInfoBirthday(filterRemove);
                deleteLocalBirthday(idNumber);
            })
    }

    return (
        <Card className={classes.root}>
            <CardContent>
                <Typography className={classes.title} color="textSecondary" gutterBottom>
                    {id}
                </Typography>
                <Typography className={classes.name} color="textPrimary">
                    {name}
                </Typography>
                <Typography variant="body2" component="p">
                    {street} ans {bull} {username}
                </Typography>
                <Typography variant="body2" component="p">
                    Cadeau : {suite}
                </Typography>
                <Typography variant="body2" component="p">
                    Prix : {city}€
                </Typography>
            </CardContent>
            <CardActions className={classes.action}>
                <Button size="small" variant="outlined" color="primary" onClick={() => setVisible(true)} >Edit</Button>
                <Modal visible={visible} width="70%" height="85%" effect="fadeInDown">
                    <div className="overflowY">
                        <div className="birthday_modal">
                            <h1>Birthday</h1>
                            <FormikYup
                                initialValues={initialValues}
                                validationSchema={validationSchema}
                                onSubmit={onSubmit}
                                setVisible={setVisible}
                            />
                            <Button className="birthday_close" onClick={() => setVisible(false)} variant="contained" color="primary">Close</Button>
                        </div>
                    </div>
                </Modal>
                <Button size="small" variant="outlined" color="primary" onClick={() => removeBirthday(id)}>Remove</Button>
            </CardActions>
        </Card>
    );
}

export default CardsBirthday