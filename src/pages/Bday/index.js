import Modal from 'react-awesome-modal';
import React, { useState, useEffect } from 'react';
import { Button } from '@material-ui/core';
import { birthdayJsonApi } from './../../components/Api';
import CardsBirthday from './Cards';
import { mapBirthday, validationSchema } from './../../util';
import { saveLocalBirthday } from './../../localStorage';
import FormikYup from './Formik';
import './styles.scss';

const Birthday = () => {
    const [visible, setVisible] = useState(false);
    const [infoBirthday, setInfoBirthday] = useState([]);
    const [infoJson, setInfoJson] = useState([]);
    let birthday = JSON.parse(localStorage.getItem('birthdayInfo'));

    let initialValues = {
        name: '',
        date: '',
        age: '',
        gift: '',
        price: ''
    }

    const onSubmit = async(values, {resetForm}) => {
        const dataValue = {
            name: values.name,
            username: values.date,
            address: {
                street: values.age,
                suite: values.gift,
                city: values.price
            }
        }

        let numberId = localStorage.getItem('numberId');
        numberId++;
        localStorage.setItem('numberId', numberId);

        await birthdayJsonApi
            .put(`/users/${numberId}`, dataValue)
            .then(response => {
                setInfoBirthday([...infoBirthday, response.data]);
                const update = mapBirthday(infoJson, numberId, dataValue);
                setInfoJson(update);
                saveLocalBirthday(response.data);
            })
            .catch(error=>console.log(error));
        resetForm({});
    }

    const getJsonApi = async() => {
        const result = await birthdayJsonApi.get(`/users`);
        setInfoJson(result.data);
    }

    useEffect(() => {
        getJsonApi();
    }, [infoBirthday]);

    return (
        <div className="birthday">
            <img src="/assets/confetti.png" className="birthday_img" alt="birthday friend people" />
            <h1>Birthday date</h1>
            <Button className="birthday_button-add" onClick={() => setVisible(true)} variant="contained" color="primary" type="reset">Add a birthday</Button>
            <Modal visible={visible} width="70%" height="85%" effect="fadeInDown">
                <div className="overflowY">
                    <div className="birthday_modal">
                        <h1>Birthday</h1>
                        <FormikYup
                            initialValues={initialValues}
                            validationSchema={validationSchema}
                            onSubmit={onSubmit}
                            setVisible={setVisible}
                        />
                        <Button className="birthday_close" onClick={() => setVisible(false)} variant="contained" color="primary">Close</Button>
                    </div>
                </div>
            </Modal>
            {birthday &&
                <div className="birthday_cards">
                    {birthday.map((info, index) => (
                        <CardsBirthday
                            key={index}
                            data={info}
                            dataAll={infoBirthday}
                            setInfoBirthday={setInfoBirthday}
                            setInfoJson={setInfoJson}
                            infoJson={infoJson}
                        />
                    ))}
                </div>
            }
        </div>
    );
};

export default Birthday;