import React from 'react';
import { ErrorMessage, useField } from 'formik';
import { TextField } from '@material-ui/core';
import './styles.scss';

export const TextFieldForm = ({ label, ...props }) => {
    const [field, meta] = useField(props);

    return (
        <div>
            <TextField
                label={field.name}
                color="primary"
                variant="outlined"
                className={`birthday_txtField ${meta.touched && meta.error && 'is-invalid'}`}
                {...field} {...props}
                autoComplete="off"
            />
            <ErrorMessage component="div" name={field.name} className="error" />
        </div>
    )
}
