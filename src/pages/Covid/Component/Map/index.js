import React from 'react';
import './styles.scss';
import { formatNumber } from './../../../../util';
import { MapContainer, Circle, Popup, TileLayer } from "react-leaflet";
import { optionsColors } from './colors';

const MapLeaflet = ({dataCountries, option= "cases"}) => {
    const position = [46.232193, 2.209667]; //France

    return (
        <div className="mapped">
           <MapContainer center={position} zoom={3} scrollWheelZoom={true}>
                <TileLayer
                    attribution='&copy;
                    <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
                {dataCountries.data.map((country, index) => (
                    <Circle
                        pathOptions={{color: optionsColors[option].hex, fillColor: optionsColors[option].hex}}
                        center={[country.countryInfo.lat,country.countryInfo.long]}
                        radius={Math.sqrt(country[option]) * optionsColors[option].multiplier}
                        key={index}
                    >
                        <Popup>
                            <img src={country.countryInfo.flag} alt="flag country" className="map_flag_img"/>
                            <h1>{country.country}</h1>
                            <div><u>Population:</u> {formatNumber(country.population)}</div>
                            <div><u>Cases:</u> {formatNumber(country.cases)}</div>
                            <div><u>Deaths:</u> {formatNumber(country.deaths)}</div>
                            <div><u>Recovered:</u> {formatNumber(country.recovered)}</div>
                        </Popup>
                    </Circle>
                ))}
            </MapContainer>
        </div>
    );
};

export default MapLeaflet;