export const optionsColors = {
    cases: {
        hex: '#ee9830',
        multiplier: 500,
    },
    todayCases: {
        hex: "#f0b56d",
        multiplier: 2500
    },
    deaths: {
        hex: "#ec443c",
        multiplier: 1500
    },
    todayDeaths: {
        hex: "#ff7069",
        multiplier: 2500
    },
    recovered: {
        hex: "#12d87e",
        multiplier: 500,
    },
    todayRecovered: {
        hex: "#7fd3ad",
        multiplier: 2500
    }
}