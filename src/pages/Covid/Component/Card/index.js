import React from 'react';
import './styles.scss';
import { formatNumber } from './../../../../util';

const CardCovid = ({data}) => {
    const { todayCases, cases, todayRecovered, recovered, todayDeaths, deaths } = data;
    const { critical, tests, population } = data;

    const cards = (name, nameClass, number1, number2) => {
        return <div className="covid_card">
            <h3>{name}</h3>
            <p className="covid_global">Global today: <span className={nameClass}>{formatNumber(number1)}</span></p>
            <h4>Total: {formatNumber(number2)}</h4>
        </div>
    }

    return (
        <div className="covid_cards_container">
            {cards("Cases", "orange", todayCases, cases)}
            {cards("Death", "red", todayDeaths, deaths)}
            {cards("Recovered", "green", todayRecovered, recovered)}
            <div className="covid_card">
                <h3>More info</h3>
                <p>Critical: {formatNumber(critical)}</p>
                <p>Tests: {formatNumber(tests)}</p>
                <p>Population: {formatNumber(population)}</p>
            </div>
        </div>
    );
};

export default CardCovid;