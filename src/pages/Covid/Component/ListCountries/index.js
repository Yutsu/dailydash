import React from 'react';
import './styles.scss';
import { formatNumber } from './../../../../util';

const ListCountries = ({data}) => {
    return (
        <ul className="list">
            {data.map((country, index) =>
                <li key={index} className="listCountries">
                    <img src={country.flag} alt="flag country" className="listCountries_img"/>
                    <div className="listCountries_info">
                        <p>{country.name}</p>
                        <div>{formatNumber(country.result)}</div>
                    </div>
                </li>
            )}
        </ul>
    );
};

export default ListCountries;