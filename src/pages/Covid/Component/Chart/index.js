import React from 'react';
import { Line } from 'react-chartjs-2';

const ChartCountry = ({dataCountry, dataVaccine}) => {
    let tabKey = [];
    let tabValue = [];

    let timeDisplay, cases, deaths, recovery, vaccined;
    let tabTime = [], tabCases = [], tabDeaths = [], tabRecovery = [], tabVaccined = [];

    const chart = (data, vaccine) => {
        for(let key in vaccine){
            let value = vaccine[key];
            tabVaccined.push({value});
        }

        for(let key in data){
            let value = data[key];
            tabKey.push({key});
            tabValue.push({value});
        }
        for(let lala in tabValue[0].value){
            tabTime.push({lala});
            let value = tabValue[0].value[lala];
            tabCases.push({value});
        }
        for(let lala in tabValue[1].value){
            let value = tabValue[1].value[lala];
            tabDeaths.push({value});
        }
        for(let lala in tabValue[2].value){
            let value = tabValue[2].value[lala];
            tabRecovery.push({value});
        }

        timeDisplay = tabTime.map(test => test.lala);
        cases = tabCases.map(test => test.value);
        deaths = tabDeaths.map(test => test.value);
        recovery = tabRecovery.map(test => test.value);
        vaccined = tabVaccined.map(test => test.value);
    }
    chart(dataCountry, dataVaccine);

    const options = {
        title: {
            display: true,
            text: `Chart population sort by country`
        },
        responsive: true,
    }
    const data = {
        labels: timeDisplay,
        datasets: [
            {
                label: "Cases",
                data: cases,
                tension: 0.1,
                borderColor: 'orange',
                backgroundColor: 'transparent',
                pointBackgroundColor: 'orange',
                pointBorderColor: 'orange',
                borderWidth: 3
            },
            {
                label: "Deaths",
                data: deaths,
                tension: 0.1,
                borderColor: 'red',
                backgroundColor: 'transparent',
                pointBackgroundColor: 'red',
                pointBorderColor: 'red',
                borderWidth: 3
            },
            {
                label: "Recovery",
                data: recovery,
                tension: 0.1,
                borderColor: 'green',
                backgroundColor: 'transparent',
                pointBackgroundColor: 'green',
                pointBorderColor: 'green',
                borderWidth: 3
            },
            {
                label: "Vaccined",
                data: vaccined,
                tension: 0.1,
                borderColor: 'blue',
                backgroundColor: 'transparent',
                pointBackgroundColor: 'blue',
                pointBorderColor: 'blue',
                borderWidth: 3
            }
        ]
    };

    return (
        <div className="chartJS">
            <Line
                options={options}
                data={data}
                height={50}
                width={100}
            />
        </div>
    );
};

export default ChartCountry;