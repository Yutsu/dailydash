import React, { useEffect, useState } from 'react';
import FormSelect from '../../components/Form/FormSelect';
import CardCovid from './Component/Card';
import ListCountries from './Component/ListCountries';
import MapLeaflet from './Component/Map';
import { covidApi } from './../../components/Api';
import { TextField } from '@material-ui/core';
import './styles.scss';
import ChartCountry from './Component/Chart';

const Covid = () => {
    //https://disease.sh/docs/
    const [all, setAll] = useState([]);
    const [listCountry, setListCountry] = useState([]);
    const [countryMap, setCountryMap] = useState([]);
    const [countryChart, setCountryChart] = useState([]);
    const [vaccineChart, setVaccineChart] = useState([]);
    const [typesOptions, setTypesOptions] = useState("cases");
    const [graphCountryOptions, setGraphCountryOptions] = useState("Global");

    const covidAll = async() => {
        const response = await covidApi.get("/all");
        setAll(response.data);
    }

    const covidCountries = async(options) => {
        const response = await covidApi.get(`/countries?sort=${options}`);
        const country = response.data.map(country => ({
            flag: country.countryInfo.flag,
            name: country.country,
            result: country[options],
        }));
        setListCountry(country);
    }

    const covidMap = async() => {
        const response = await covidApi.get('/countries');
        setCountryMap(response);
    }

    const types = [
        { value: 'cases', label: "Cases" },
        { value: "todayCases", label: "Today cases" },
        { value: "deaths", label: "Deaths" },
        { value: "todayDeaths", label: "Today deaths" },
        { value: "recovered", label: "Recovered" },
        { value: "todayRecovered", label: "Today recovered" }
    ];

    useEffect(() => {
        covidAll();
        covidCountries(typesOptions);
        covidMap();
        const covidChart = async() => {
            if(graphCountryOptions !== "Global"){
                const result = await covidApi.get(`/historical/${graphCountryOptions}?lastdays=30`);
                const resultVaccine = await covidApi.get(`/vaccine/coverage/countries/${graphCountryOptions}?lastdays=30`);
                setCountryChart(result.data.timeline);
                setVaccineChart(resultVaccine.data.timeline);
    
            } else {
                const responseOne = await covidApi.get('/historical/all');
                const respVaccine = await covidApi.get(`/vaccine/coverage?lastdays=30`);
                setCountryChart(responseOne.data);
                setVaccineChart(respVaccine.data);
            }
        }
        covidChart();
    }, [typesOptions, graphCountryOptions]);

    return (
        <div className="covid">
            { all && listCountry && <>
                <h2>Covid19 Tracker</h2>
                <div className="covid_container">
                    <div className="covid_cards">
                        <CardCovid data={all}/>
                    </div>
                    <div className="covid_list">
                        <div className="list-options">
                            <h2>List countries</h2>
                            <FormSelect
                                text={"t-right"}
                                options={types}
                                onChange={e => setTypesOptions(e.target.value)}
                                value={typesOptions}
                                type="reset"
                                label="Options"
                            />
                        </div>
                        <ListCountries data={listCountry} />
                    </div>
                    <div className="covid_map">
                        {countryMap.length !== 0 &&
                            <MapLeaflet
                                dataCountries={countryMap}
                                option={typesOptions}
                            />
                        }
                    </div>
                </div>
                <div className="covid_chart">
                    <TextField
                        id="outlined-select-currency-native"
                        select
                        options={listCountry}
                        SelectProps={{native: true}}
                        variant="outlined"
                        onChange={e => setGraphCountryOptions(e.target.value)}
                        className="select"
                        value={graphCountryOptions}
                        type="reset"
                    >
                        <option> Global </option>
                        {listCountry.map((option, index) => (
                            <option key={index} value={option.name}>
                                {option.name}
                            </option>
                        ))}
                    </TextField>
                    {countryChart.length !== 0 &&
                        <ChartCountry
                            dataCountry={countryChart}
                            dataVaccine={vaccineChart}
                        />
                    }
                </div>
            </>}
        </div>
    );
};

export default Covid;