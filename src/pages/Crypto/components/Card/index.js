import React from 'react';
import Card from '@material-ui/core/Card';
import './styles.scss';
import { Link } from 'react-router-dom';
import { formatNumber, displayPercentage } from './../../../../util';

const CardCrypto = ({dataCrypto}) => {
    const { name, id, image, current_price, symbol } = dataCrypto
    const { circulating_supply, price_change_percentage_24h, total_volume } = dataCrypto

    return (
        <Card className="card_crypto">
            <div className="line">
                <div className="logo_name">
                    <img className="img_crypo" src={image} alt="crypto"/>
                    <Link to={`/coins/${id}`} >
                        <p>{name}</p>
                    </Link>
                </div>
                {displayPercentage(price_change_percentage_24h)}
            </div>
            <p>Price : {formatNumber(current_price.toFixed(2))}€</p>
            <p>Supply : {formatNumber(circulating_supply.toFixed(2))}</p>
            <p>Symbol : <span className="crypto_symbol">{symbol}/eur</span></p>
            <p>Total volume : {total_volume.toLocaleString()}€</p>
        </Card>
    );
};

export default CardCrypto;