import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { Button, TablePagination } from '@material-ui/core';
import { Link } from 'react-router-dom';
import { formatNumber, displayPercentage } from './../../../../util';

const displayGraph = (urlImg) => {
    if(urlImg){
        let result = urlImg.split('/');
        return `https://www.coingecko.com/coins/${result[5]}/sparkline`
    }
}

const columns = [
    {
        id: "Logo",
        label: 'image',
        minWidth: 100,
        format: (value) => <img className="crypto_logo_img" src={value} alt="logo crypto"></img>
    },
    {
        id: 'Name',
        label: 'name',
        minWidth: 100
    },
    {
        id: 'Price',
        label: 'current_price',
        minWidth: 100,
        format: (value) => formatNumber(value.toFixed(2)) + "€"
    },
    {
        id: 'Percentage (24h)',
        label: 'price_change_percentage_24h',
        minWidth: 100,
        format: (value) => displayPercentage(value.toFixed(2))
    },
    {
        id: 'Total volume',
        label: 'total_volume',
        minWidth: 100,
        format: (value) => value.toLocaleString() + "€"
    },
    {
        id: 'Graph (7d)',
        label: 'image',
        minWidth: 100,
        format: (value) => <img className="crypto_logo_img" src={displayGraph(value)} alt="logo crypto"></img>,
    },
    {
        id: 'Detail',
        label: 'id',
        minWidth: 100,
        format: (value) => <Link to={`/coins/${value}`} >
            <Button variant="contained" color="primary">See more</Button>
        </Link>,
    },
];

const useStyles = makeStyles({
    root: {
        width: '100%',
    },
    container: {
        maxHeight: 450,
    },
    cell: {
        backgroundColor: "black",
        color: "white"
    }

});

const TableCrypto = ({dataTableCrypto}) => {
    const classes = useStyles();
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
      };

      const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(+event.target.value);
        setPage(0);
      };

    return (
        <Paper className={classes.root}>
            <TableContainer className={classes.container}>
                <Table stickyHeader aria-label="sticky table">
                    <TableHead>
                        <TableRow>
                        {columns.map((column, key) => (
                            <TableCell
                                className={classes.cell}
                                key={key}
                                align="center"
                                style={{minWidth: column.minWidth}}
                            >
                                {column.id}
                            </TableCell>
                        ))}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {dataTableCrypto.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row, index) => {
                        return (
                            <TableRow hover role="checkbox" tabIndex={-1} key={index}>
                            {columns.map((column, index1) => {
                                const value = row[column.label];
                                return (
                                <TableCell key={index1} align="center">
                                    {column.format ? column.format(value) : value}
                                </TableCell>
                                );
                            })}
                            </TableRow>
                        );
                        })}
                    </TableBody>
                </Table>
            </TableContainer>
            <TablePagination
                rowsPerPageOptions={[10, 20]}
                component="div"
                count={dataTableCrypto.length}
                rowsPerPage={rowsPerPage}
                page={page}
                onChangePage={handleChangePage}
                onChangeRowsPerPage={handleChangeRowsPerPage}
            />
        </Paper>
    );
};

export default TableCrypto;