import React, { useEffect, useState } from 'react';
import { useHistory, useParams } from 'react-router';
import { coinGecko } from './../../../../components/Api';
import ChartCrypto from '../Chart';
import { FiArrowLeft } from "react-icons/fi";
import './styles.scss';
import { Button } from '@material-ui/core';

const DetailPage = () => {
    const {id} = useParams();
    const [coinData, setCoinData] = useState({});
    const [isLoading, setIsLoading] = useState(false);
    let history = useHistory();

    const formatData = (data) => {
        return data.map(element => {
            return {
                t: element[0],
                y: element[1].toFixed(2),
            }
        })
    }

    useEffect(() => {
        //Promise : charge all in one (Network / XHR)
        window.scrollTo(0, 0);
        const fetchData = async() => {
            const [day, week, year, detail, crypto] = await Promise.all([
                coinGecko.get(`/coins/${id}/market_chart`, {
                    params: {
                        vs_currency: "eur",
                        days: "1",
                    }
                }),
                coinGecko.get(`/coins/${id}/market_chart`, {
                    params: {
                        vs_currency: "eur",
                        days: "7",
                    }
                }),
                coinGecko.get(`/coins/${id}/market_chart`, {
                    params: {
                        vs_currency: "eur",
                        days: "365",
                    }}
                ),
                coinGecko.get("/coins/markets", {
                    params: {
                        vs_currency: "eur",
                        ids: id
                    }
                }),
                coinGecko.get(`/coins/${id}`, {
                    params: {
                        ids: id
                    }}
                ),
            ]);

            setCoinData({
                day: formatData(day.data.prices),
                week: formatData(week.data.prices),
                year: formatData(year.data.prices),
                detail: detail.data[0],
                crypto: crypto.data
            });
        }

        setIsLoading(true);
        fetchData();
        setIsLoading(false);

    }, [id])

    const renderData = () => {
        if(isLoading){
            return <div className="loading">
                <h1>Loading ...</h1>
                <img src="/assets/loading.svg" className="loading_img" alt="loading page"/>
            </div>
        }
        return (
            <div className="container-detail">
                <div className="button_return">
                    <Button onClick={history.goBack} variant="contained" color="primary"><FiArrowLeft />  Return </Button>
                </div>
                <ChartCrypto data={coinData}/>
            </div>
        );
    }
    return renderData();

};

export default DetailPage;