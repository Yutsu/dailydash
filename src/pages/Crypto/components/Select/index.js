import { TextField } from '@material-ui/core';
import React from 'react';

const FormSelectCrypto = ({ options, onChange, label, value, text, ...props}) => {
    if (!Array.isArray(options) || options.length < 1) return null;

    return (
        <div>
            <TextField
                id="outlined-select-currency-native"
                select
                options={options}
                label={label}
                SelectProps={{native: true}}
                variant="outlined"
                onChange={onChange}
                value={value}
                { ...props}
            >
                <option> </option>
                {options.map((option, index) => (
                    <option key={index} value={option}>
                        {option.toUpperCase()}
                    </option>
                ))}
            </TextField>
        </div>
    );
}

export default FormSelectCrypto;