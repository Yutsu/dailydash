import React from 'react';
import './styles.scss';
import { FaTwitter, FaRegThumbsDown, FaRegThumbsUp } from "react-icons/fa";
import { FcReddit } from "react-icons/fc";
import { FiGithub } from "react-icons/fi";
import { CgCommunity } from "react-icons/cg";
import { MdDateRange } from "react-icons/md";
import { RiComputerLine } from "react-icons/ri";

const Information = ({data}) => {
    const  { community_score, genesis_date, sentiment_votes_down_percentage, sentiment_votes_up_percentage, description } = data;
    const  { twitter_followers, reddit_subscribers, reddit_accounts_active_48h,reddit_average_comments_48h, reddit_average_posts_48h } = data.community_data;
    const  { forks, pull_request_contributors, pull_requests_merged, stars, subscribers, total_issues } = data.developer_data;
    const  { subreddit_url, repos_url } = data.links;

    const displayDate = (date) => {
        if(date === null){
            return <p><MdDateRange /> <u>Date:</u> x</p>
        }
        return <p><MdDateRange/> <u>Date:</u> {date}</p>
    }

    return (
        <div>
            <div className="crypto_img">
                <img src={data.image.large} alt="crypto monnaie display"/>
            </div>
            <div className="thumbs">
                <p><FaRegThumbsDown className="red"/> {sentiment_votes_down_percentage}%</p>
                <p><FaRegThumbsUp className="green"/> {sentiment_votes_up_percentage}%</p>
            </div>
            <div className="score_date">
                {displayDate(genesis_date)}
                <p><CgCommunity /> <u>Community Score:</u> {community_score}</p>
                <p><FaTwitter /> <u>Twitter:</u> {twitter_followers.toLocaleString()} followers</p>
            </div>
            <div className="text" dangerouslySetInnerHTML={{ __html: description.en }} />
            <div className="reddit_dev">
                <div className="reddit">
                    <h3><FcReddit /> Reddit</h3>
                    <p>Subscribers : {reddit_subscribers.toLocaleString()}</p>
                    <p>Active account (48h): {reddit_accounts_active_48h.toLocaleString()}</p>
                    <p>Average comment (48h): {reddit_average_comments_48h}</p>
                    <p>Average posts (48h): {reddit_average_posts_48h}</p>
                    <p>Link : <a href={subreddit_url} className="black">{subreddit_url}</a></p>
                </div>
                <div className="dev">
                    <h3><RiComputerLine /> Developer</h3>
                    <p>Subscriber: {subscribers.toLocaleString()}</p>
                    <p>Stars: {stars.toLocaleString()}</p>
                    <p>Forks: {forks.toLocaleString()}</p>
                    <p>Pull request contributors: {pull_request_contributors.toLocaleString()}</p>
                    <p>Merged: {pull_requests_merged.toLocaleString()}</p>
                    <p>Total issues: {total_issues.toLocaleString()}</p>
                    <div><FiGithub />Repos github:{repos_url.github.map((test, index) =>
                        <div key={index} className="black">
                            <a href={test} target="_blank" rel="noreferrer">{test}</a><br/>
                        </div>)}
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Information;