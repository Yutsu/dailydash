import React, { useEffect, useRef, useState } from 'react';
import Chartjs from 'chart.js';
import './styles.scss';
import { historyOptions } from './chartConfigs';
import { Button } from '@material-ui/core';
import Information from '../Information';
import { formatNumber, displayPercentage } from './../../../../util';

const ChartCrypto = ({data}) => {
    const chartRef = useRef();
    const { day, week, year, detail, crypto } = data;
    const [timeFormat, setTimeFormat] = useState("24h");

    const timeFormatDisplay = () => {
        switch(timeFormat){
            case "24h":
                return day;
            case "7day":
                return week;
            case "1year":
                return year;
            default:
                return day;
        }
    };

    useEffect(() => {
        if(chartRef && chartRef.current && detail){
            const chartInstance = new Chartjs(chartRef.current, {
                type: 'line',
                data: {
                    datasets: [{
                        label: `Prices`,
                        data: timeFormatDisplay(),
                        backgroundColor: "lightblue",
                        borderColor: "orange",
                        pointRadius: 0,
                    }]
                },
                options: { ...historyOptions},
            });
            console.log(chartInstance)
        }
    });

    return (
        <div className="chart">
            {!detail && <div className="loading">
                <h1>Loading ...</h1>
                <img src="/assets/loading.svg" className="loading_img" alt="loading page"/>
            </div>}
            {detail && <>
                <h1 className="chart_title_name">{detail.id}</h1>
                <div className="chart_price_percentage">
                    <p className="chart_text"><u>Now:</u> {formatNumber(detail.current_price.toFixed(2))}$</p>
                    {displayPercentage(detail.price_change_percentage_24h.toFixed(2))}
                </div>
                <div className="chart_graph">
                    <canvas ref={chartRef} id="myChart" width={50} height={50}></canvas>
                </div>
                <div className="chart_button_list">
                    <Button className="chart_button" onClick={ () => setTimeFormat("24h")} variant="contained" color="primary">24h</Button>
                    <Button className="chart_button" onClick={ () => setTimeFormat("7day")} variant="contained" color="primary">Week</Button>
                    <Button className="chart_button" onClick={ () => setTimeFormat("1year")} variant="contained" color="primary">Year</Button>
                </div>
                <Information data={crypto}/>
            </>}
        </div>
    );
};

export default ChartCrypto;