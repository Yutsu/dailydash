import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import './styles.scss';

const useStyles = makeStyles({
    table: {
        marginTop: "10px",
        minWidth: 650,
    },
    row: {
        backgroundColor: "black",
        color: "white"
    }
  });

const Exchange = ({dataExchange}) => {
    const classes = useStyles();

    const displayDate = (date) => {
        if(date === null){
            return <p> X </p>
        }
        return <p> {date} </p>
    }

    return (
        <div className="exchange">
            <h1>Top 5 exchange</h1>
            <TableContainer component={Paper}>
                <Table className={classes.table} size="small" aria-label="a dense table">
                    <TableHead>
                    <TableRow>
                        <TableCell className={classes.row} align="center">Logo</TableCell>
                        <TableCell className={classes.row} align="center">Name</TableCell>
                        <TableCell className={classes.row} align="center">Year</TableCell>
                        <TableCell className={classes.row} align="center">Link</TableCell>
                    </TableRow>
                    </TableHead>
                    <TableBody>
                    {dataExchange.map((row) => (
                        <TableRow key={row.name}>
                        <TableCell align="center"><img src={row.image} alt="logo crypto"></img></TableCell>
                        <TableCell align="center">{row.name}</TableCell>
                        <TableCell align="center">{displayDate(row.year_established)}</TableCell>
                        <TableCell align="center"><a href={row.url} className="black" target="_blank" rel="noreferrer">{row.url}</a></TableCell>
                        </TableRow>
                    ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </div>
    );
};

export default Exchange;