import React, {useState, useEffect} from 'react';
import './styles.scss';
import axios from 'axios';
import FormInput from '../../../../components/Form/FormInput';
import { FaEquals } from "react-icons/fa";
import { coinGecko } from './../../../../components/Api';
import { formatNumber } from './../../../../util';
import FormSelectCrypto from '../Select';

const Converter = () => {
    const [selectOptions, setSelectOptions] = useState([]);
    const [selectOne, setSelectOne] = useState("");
    const [selectTwo, setSelectTwo] = useState("");
    const [amount, setAmount] = useState("");
    const [result, setResult] = useState("");

    const getCurrencies = async() => {
        const response = await coinGecko.get("/simple/supported_vs_currencies")
        setSelectOptions(response.data);
    }

    useEffect(() => {
        getCurrencies();
        const getConvertion = async() => {
            await axios
            .get(`https://min-api.cryptocompare.com/data/price?fsym=${selectOne.toUpperCase()}&tsyms=${selectTwo.toUpperCase()}`)
            .then(result => {
                if(result){
                    let total = result.data[selectTwo.toUpperCase()] * amount;
                    setResult(total.toFixed(2));
                }
            })
        }
        getConvertion()
    },[amount, selectOne, selectTwo])

    return (
        <div className="converter">
            <h1 className="converter_title">Currency calculator</h1>
            <div className="converter_field">
                <div className="field">
                     <FormSelectCrypto
                        options={selectOptions}
                        onChange={e => setSelectOne(e.target.value)}
                        value={selectOne}
                        type="reset"
                        label="From"
                    />
                    <FaEquals />
                    <FormSelectCrypto
                        options={selectOptions}
                        onChange={e => setSelectTwo(e.target.value)}
                        value={selectTwo}
                        type="reset"
                        label="To"
                    />
                    <FormInput
                        label="Amount"
                        type="number"
                        name="amount"
                        onChange={e => setAmount(e.target.value)}
                        value={amount}
                    />
                </div>
                {result && amount && selectOne && selectTwo &&
                    <div className="converter_result">
                        {formatNumber(amount)} {selectOne.toUpperCase()} = {formatNumber(result)} {selectTwo.toUpperCase()}
                    </div>
                }
            </div>
        </div>
    );
};

export default Converter;