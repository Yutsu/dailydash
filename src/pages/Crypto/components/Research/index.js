import React, { useState } from 'react';
import FormInput from '../../../../components/Form/FormInput';
import { coinGecko } from './../../../../components/Api';
import axios from 'axios';
import './styles.scss';
import { Link } from 'react-router-dom';
import { Button } from '@material-ui/core';

const Research = () => {
    const [research, setResearch] = useState("");
    const [infoResearch, setInfoResearch] = useState([]);
    const [catchError, setCatchError] = useState(false);

    const handleFormSubmit = async(e) => {
        e.preventDefault();
        await axios
            .get(`${coinGecko.defaults.baseURL}/coins/${research.toLowerCase()}`)
            .then(response => {
                setInfoResearch(response.data);
                setCatchError(false);
            })
            .catch(error => {
                if(error.response && error.response.status === 404){
                    setInfoResearch([]);
                    setCatchError(true);
                    console.clear();
                }
            })
            setResearch("");
    }

    return (
        <div>
            <h1>Research</h1>
            <div className="research">
                <form onSubmit={handleFormSubmit}>
                    <FormInput
                        label="Search coin"
                        type="text"
                        name="Coin"
                        value={research}
                        onChange={e => setResearch(e.target.value)}
                    />
                </form>
                {infoResearch.length === 0 && !catchError && <img src="/assets/research.svg" className="research_img" alt="research page"/>}
                {infoResearch.length !== 0 && !catchError && <div className="research_info">
                    <img src={infoResearch.image.small} alt="crypto"/>
                    <p>CoinGecko rank : {infoResearch.coingecko_rank}</p>
                    <p>Market cap rank : {infoResearch.market_cap_rank}</p>
                    <div className="research_description" dangerouslySetInnerHTML={{ __html: infoResearch.description.en }} />
                    <Link to={`/coins/${infoResearch.id}`} >
                        <Button variant="contained" color="primary">See more</Button>
                    </Link>
                </div>}
                {catchError && <div className="img_not_found">
                    <img src="/assets/not-found.png" className="not_found" alt="error not found"/>
                </div>}
            </div>
        </div>
    );
};

export default Research;