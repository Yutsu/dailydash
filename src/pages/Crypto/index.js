import React, { useState, useEffect } from 'react';
import CardCrypto from './components/Card';
import TableCrypto from './components/Table';
import './styles.scss';
import Exchange from './components/Exchange';
import Converter from './components/Converter';
import Research from './components/Research';
import { coinGecko } from './../../components/Api';

const Crypto = () => {
    const [cryptoInfo, setCryptoInfo] = useState([]);
    const [tabCryptoInfo, setTabCryptoInfo] = useState([]);
    const [tabExchange, setTabExchange] = useState([]);
    const [isLoading, setIsLoading] = useState(false);

    const cards = async() => {
        setIsLoading(true);
        const response = await coinGecko.get("/coins/markets", {
            params: {
                vs_currency: "eur",
                ids: "ravencoin, dogecoin, bitcoin, pancakeswap-token",
                sparkline: false,
            }
        })
        setCryptoInfo(response.data)
    }

    const table = async() => {
        const response = await coinGecko.get("/coins/markets", {
            params: {
                vs_currency: "eur",
                order: "market_cap_desc",
                page: 1,
                sparkline: false,
            }
        })
        setTabCryptoInfo(response.data)
    }

    const exchange = async() => {
        const response = await coinGecko.get("/exchanges", {
            params: {
                per_page: 5
            }
        })
        setTabExchange(response.data);
        setIsLoading(false);
    }

    useEffect(() => {
        cards();
        table();
        exchange();
    },[])

    return (
        <>
            {isLoading && <div className="loading">
                <h1>Loading ...</h1>
                <img src="/assets/loading.svg" className="loading_img" alt="loading page"/>
            </div>}
            {!isLoading && cryptoInfo && tabCryptoInfo && <div className="crypto_container">
                <div className="crypto_title">
                    <img src="/assets/crypto.png" className="crypto_logo" alt="cryptoCurrency mine coins"/>
                    <h2>CryptoMine</h2>
                </div>
                <section className="crypto_cards">
                    {cryptoInfo.map((crypto, index) => (
                        <CardCrypto key={index} dataCrypto={crypto}/>
                    ))}
                </section>
                <section className="crypto_table">
                    <TableCrypto dataTableCrypto={tabCryptoInfo} />
                </section>
                <section className="crypto_converter">
                    <Converter />
                </section>
                <section className="crypto_exchange_research">
                    <Exchange dataExchange={tabExchange}/>
                    <Research />
                </section>
            </div>}
        </>
    );
};

export default Crypto;