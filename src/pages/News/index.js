import { Button } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import { spaceApi } from './../../components/Api';
import CardNews from './components/Card';
import './styles.scss';

const News = () => {
    const [articles, setArticles] = useState([]);
    const [blogs, setBlogs] = useState([]);
    const [reports, setReports] = useState([]);
    const [clickButton, setClickButton] = useState("articles");
    const [isLoading, setIsLoading] = useState(false);

    const newsArticle = async() => {
        setIsLoading(true)
        const result = await spaceApi.get('/articles?_limit=30');
        setArticles(result.data);
    }

    const newsBlogs = async() => {
        const result = await spaceApi.get('/blogs?_limit=30');
        setBlogs(result.data);
    }

    const newsReports = async() => {
        const result = await spaceApi.get('/reports?_limit=30');
        setReports(result.data);
        setIsLoading(false);
    }

    const switchDisplay = (options) => {
        switch(options) {
            case 'articles':
                return <>
                    {articles && articles.map((test, index) => (
                        <CardNews data={test} key={index} />
                    ))}
                </>
            case 'blogs':
                return <>
                    {blogs && blogs.map((test, index) => (
                        <CardNews data={test} key={index} />
                    ))}
                </>
            case 'reports':
                return <>
                    {reports && reports.map((test, index) => (
                        <CardNews data={test} key={index} />
                    ))}
                </>
            default:
                return null;
        }
    }

    useEffect(() => {
        newsArticle();
        newsBlogs();
        newsReports();
    }, []);

    return (
        <div className="news">
            {isLoading && <div className="loading">
                <h1>Loading ...</h1>
                <img src="/assets/loading.svg" className="loading_img" alt="loading page"/>
            </div>}
            <h2>Spaceflight News</h2>
            <img src="/assets/thomas.jpg" alt="tomas pesquet astro" className="news_img"/>
            <div className="news_button_list">
                <Button className="news_button" onClick={() => setClickButton("articles")} variant="contained" color="primary">Articles</Button>
                <Button className="news_button" onClick={() => setClickButton("blogs")} variant="contained" color="primary">Blog</Button>
                <Button className="news_button" onClick={() => setClickButton("reports")} variant="contained" color="primary">Report</Button>
            </div>
            <div className="news_card">
                {switchDisplay(clickButton)}
            </div>
        </div>
    );
};

export default News;