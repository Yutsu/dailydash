import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import moment from 'moment';
import Typography from '@material-ui/core/Typography';
import './styles.scss';

const useStyles = makeStyles({
    root: {
      maxWidth: 345,
      position: "relative",
      height: "100%",
    },
    media: {
      height: 180,
    },
    actions: {
        position: "absolute",
        bottom: 0,
        display: "flex",
        justifyContent: "space-between",
        width: "100%"
    },
    title: {
        color: "black"
    }
});

const CardNews = ({data}) => {
    const classes = useStyles();

    return (
        <div className="card_info">
            <Card className={classes.root}>
                <CardActionArea>
                    <a href={data.url} target="_blank" rel="noreferrer">
                        <CardMedia
                            className={classes.media}
                            image={data.imageUrl}
                            title={data.id}
                        />
                        <CardContent>
                            <Typography gutterBottom variant="h5" component="h2" className={classes.title}>
                                {data.title}
                            </Typography>
                            <Typography className="card_info_news"variant="body2" color="textSecondary" component="p">
                                {data.summary}
                            </Typography>
                        </CardContent>
                    </a>
                </CardActionArea>
                <CardActions className={classes.actions}>
                    <Typography variant="body2" color="textSecondary" component="p">
                        <u>Source:</u> {data.newsSite}
                    </Typography>
                    <Typography variant="body2" color="textSecondary" component="p">
                        <u>Date:</u> {moment(data.publishedAt).format('L')}
                    </Typography>
                </CardActions>
            </Card>
        </div>
    );
};

export default CardNews;