import { createStore, combineReducers } from 'redux';
import cityFavReducer from './Meteo/reducers/cityFav';
import navReducer from './Nav/reducers/nav';

const rootReducer = combineReducers({
    cityFav: cityFavReducer,
    navClick: navReducer
});

const store = createStore(
    rootReducer,
    window.__REDUX_DEVTOOLS_EXTENSION__ &&
    window.__REDUX_DEVTOOLS_EXTENSION__()
);

export default store;