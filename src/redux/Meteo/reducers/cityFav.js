import cityFavTypes from '../types/cityFav';

const INITIAL_STATE = {
    cityDisplay: []
}

const cityFavReducer = (state = INITIAL_STATE, action) => {
    switch(action.type){
        case cityFavTypes.TOGGLE_CITY_FAV:
            if(action.payload.isFav === true){
                console.log("Reducer: " + action.payload.city + ", " + action.payload.isFav);
                return {
                    ...state,
                    cityDisplay: [
                        action.payload,
                        ...state.cityDisplay,
                    ]
                };
            }
            if(action.payload.isFav === false){
                return {
                    ...state,
                    cityDisplay: state.cityDisplay.filter(nameCity => nameCity.city !== action.payload.city)
                }
            }
            return state;
        case cityFavTypes.REMOVE_CITY_FAV:
            return {
                ...state,
                cityDisplay: state.cityDisplay.filter(nameCity => nameCity.city !== action.payload.city )
            }
        default:
            return state;
    }
}

export default cityFavReducer;