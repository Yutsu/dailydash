const cityFavTypes = {
    TOGGLE_CITY_FAV: 'TOGGLE_CITY_FAV',
    REMOVE_CITY_FAV: 'REMOVE_CITY_FAV'
}

export default cityFavTypes;