import cityFavTypes from '../types/cityFav';

export const toggleCityAction = ({name, favCity, countryCity}) => ({
    type: cityFavTypes.TOGGLE_CITY_FAV,
    payload: {
        city: name,
        isFav: !favCity,
        country: countryCity
    }
});

export const removeCityFavAction = ({cityClick}) => ({
    type: cityFavTypes.REMOVE_CITY_FAV,
    payload: { city: cityClick }
})
