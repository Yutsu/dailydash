const navTypes = {
    TOGGLE_NAV_BIG: 'TOGGLE_NAV_BIG',
    TOGGLE_NAV_SMALL: 'TOGGLE_NAV_SMALL'
}

export default navTypes;