import navTypes from '../types/nav';

const INITIAL_STATE = {
    big:    [false],
    small:  [false]
}

const navReducer = (state = INITIAL_STATE, action) => {
    switch(action.type){
        case navTypes.TOGGLE_NAV_BIG:
            return {
                ...state,
                big: action.payload
            };
        case navTypes.TOGGLE_NAV_SMALL:
            return {
                ...state,
                small: action.payload
            };
        default:
            return state;
    }
}

export default navReducer;