import navTypes from '../types/nav';

export const toggleNavBigAction = (toggleBigScreen) => ({
    type: navTypes.TOGGLE_NAV_BIG,
    payload: !toggleBigScreen
});

export const toggleNavSmallAction = (toggleSmallScreen) => ({
    type: navTypes.TOGGLE_NAV_SMALL,
    payload: !toggleSmallScreen
});