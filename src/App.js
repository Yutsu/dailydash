import { Route, Switch } from "react-router";
import VerticalNav from "./components/VerticalNav";
import MainLayout from "./layout/MainLayout";

//pages
import Crypto from "./pages/Crypto";
import News from "./pages/News";
import Meteo from "./pages/Meteo";
import Covid from "./pages/Covid";
import Birthday from "./pages/Bday";
import DetailPage from "./pages/Crypto/components/Detail";

function App() {
    return (
        <div className="App">
            <VerticalNav />
            <Switch>
                <Route exact path="/" render={() => (
                    <MainLayout>
                        <News />
                    </MainLayout>
                )}/>
                <Route path="/meteo" render={() => (
                    <MainLayout>
                        <Meteo />
                    </MainLayout>
                )}/>
                <Route path="/crypto" render={() => (
                    <MainLayout>
                        <Crypto />
                    </MainLayout>
                )}/>
                <Route path="/coins/:id" render={() => (
                    <MainLayout>
                        <DetailPage />
                    </MainLayout>
                )}/>
                <Route path="/covid" render={() => (
                    <MainLayout>
                        <Covid />
                    </MainLayout>
                )}/>
                <Route path="/bday" render={() => (
                    <MainLayout>
                        <Birthday />
                    </MainLayout>
                )}/>
            </Switch>
        </div>
    );
}

export default App;
