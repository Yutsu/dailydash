import React from 'react';
import Footer from '../components/Footer';
import Header from '../components/Header';
import { useSelector } from 'react-redux';
import './styles.scss';

const mapState = ({navClick}) => ({
    bigNav: navClick.big,
});

const MainLayout = (props) => {
    const { bigNav } = useSelector(mapState);

    return (
        <div className={bigNav ? "mainLayout" : "mainLayout toggleBig"}>
            <Header />
            <div className="main">
                {props.children}
            </div>
            <Footer />
        </div>
    );
};

export default MainLayout;