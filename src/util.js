import * as Yup from 'yup';

export const formatNumber = (num) => {
    if(num){
        return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')
    }
    if(num === 0) return "0";
}

export const displayPercentage = (price) => {
    const percentage = Math.round(100*price)/100;
    const display = <div className={percentage > 0 ? "green" : "red"}>
        <p>{percentage}%</p>
    </div>
    return display;
}

export const mapBirthday = (tab, id, result) => {
    return tab.map(info => {
        if (info.id === id) {
            info.name = result.name;
            info.username = result.username;
            info.address.street = result.address.street
            info.address.suite = result.address.suite
            info.address.city = result.address.city
        }
        return info;
    })
}

export const validationSchema = Yup.object({
    name: Yup.string()
        .max(30, 'Name must be 30 characters or less')
        .required('Name is required'),
    date: Yup.string()
        .min(5, 'Date must be 5 characters or less')
        .required('Date is required'),
    age: Yup.number()
        .typeError('Age must be a number')
        .positive('Age must be greater than zero')
        .required('Age is required'),
    gift: Yup.string()
        .max(50, 'Gift must be 50 charaters or less')
        .required('Gift is required'),
    price: Yup.number()
        .typeError('Price must be a number')
        .positive('Price must be greater than zero')
        .required('Price is required'),
});
